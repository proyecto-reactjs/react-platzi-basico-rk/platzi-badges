# Proyecto del Curso de React.js 2019

## Platzi Badges

Instruido por:

Richard Kaufman

Descripción:

Crearás una plataforma para llevar registro de los asistentes a la Platzi Conf, haciendo posible que cada usuario registrado pueda ingresar su información personal. Cada uno de ellos tendrá un rol diferente y esto se verá reflejado en su badge. Por último, crearás una página que permitirá listar a todos los participantes y filtrarlos por tipo de rol, o hacer búsqueda por nombre.

![Portada](https://i.imgur.com/k75fK3V.png)

## Nota:

creado con:

`npx create-react-app`

### Comandos:

- `npm install --save react-router-dom`
- `npm install md5`
- `npm install bootstrap`
- `npm i faker -D`
- `npm i json-server -D`
- `npm i npm-run-all -D`

### Colocar en package.json:

```
  "scripts": {
    "start": "npm-run-all -p client server",
    "client": "react-scripts start",
    "server": "json-server --port 3001 --watch server/db.json",
    "seed": "node server/seed.js",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject"
  },
```
